import pycel

"""
note vlookup doesn't work with fourth arg False -> tries to parse as range
"""

def compute(xlsx, inputs, outputs):
    """
        1. fill in inputs 
        2. run XLSX (app.xlsx)
        3. return outputs 
    """
    # excel = pycel.ExcelCompiler(filename=XLSX)
    excel = pycel.ExcelCompiler(filename=xlsx)
    #
    # populate excel.cell_map
    formula_cells = excel.formula_cells()
    if True:
        values = excel.evaluate(formula_cells)
        #
        # set all input values
        for i in inputs:
            cell_address = f"{i['sheet']}!{i['cell']}"
            value = i['value']
            if not cell_address in excel.cell_map:
                print(f"{cell_address} not found")
                excel.cell_map[cell_address] = excel.Cell(address=cell_address) 
            print(cell_address)
            excel.set_value(cell_address, value)
    #
    # compute
    values = excel.evaluate(formula_cells)
    address_to_value = {c.address: v for c, v in zip(formula_cells, values)}
    #
    # extract all output values
    computed_outputs = []
    for o in outputs:
        cell_address = f"{o['sheet']}!{o['cell']}"
        value = address_to_value.get(cell_address)
        computed_outputs.append({"name": o["name"], "value": value})
    #
    return computed_outputs
