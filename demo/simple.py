import json

import excel


def simpleExcel(inputs):
    with open("demo/inputs.json") as f:
        input_schema = json.load(f)
    #
    with open("demo/outputs.json") as f:
        output_schema = json.load(f)
    #
    filled_inputs = []
    for i in input_schema:
        if i["name"] in inputs:
            value = inputs[i["name"]]
        else:
            value = i["default"]
        filled_inputs.append({
            **i,
            'value': value
        })
    return filled_inputs, output_schema


if __name__ == '__main__':
    XLSX = "demo/Financial Calculator.xlsx"
    i, o = simpleExcel({"retail area": 100, "commercial area": 300})
    print(excel.compute(XLSX, i, o))

