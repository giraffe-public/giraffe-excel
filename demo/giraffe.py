import json
import os
import base64
import tempfile

import requests
import excel
from jinja2 import Environment, BaseLoader
from openpyxl import Workbook, load_workbook

XLSX_FILE = os.environ.get("XLSX_FILE", "demo/Financial Calculator.xlsx")


def encodeXLSX(XLSX, inputs):
    """
        fill XLSX with inputs then return b64 encoded file
    """
    wb = load_workbook(XLSX)
    for i in inputs:
        wb[i["sheet"]][i["cell"]] = i["value"]
    with tempfile.NamedTemporaryFile() as f:
        wb.save(f.name)
        f.seek(0)
        content = f.read()
    data = base64.b64encode(content).decode()
    media_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    b64 = f"data:{media_type};base64,{data}"
    return b64


def giraffeToExcel(payload):
    """
        return excel inputs from Giraffe data
    """
    features = [
        f
        for f in payload["giraffeModel"]["features"]
        if f["properties"]["type"] != "projectBoundary"
    ]
    # you can use ranges but get's harder to debug, and adding Cell does't work to add cells - would CellRange?
    ids = [{
        "value": f["properties"]["id"],
        "cell": f"A{2 + i}",
        "sheet": "giraffe sections",
    } for i, f in enumerate(features)]
    usages = [{
        "value": f["properties"]["usage"],
        "cell": f"B{2 + i}",
        "sheet": "giraffe sections",
    } for i, f in enumerate(features)]
    areas = [{
        "value": f["properties"]["area"],
        "cell": f"C{2 + i}",
        "sheet": "giraffe sections",
    } for i, f in enumerate(features)]
    #
    inputs = ids + usages + areas
    #
    with open("demo/outputs.json") as f:
        output_schema = json.load(f)
    return inputs, output_schema


def makeHTML(appOutputs, b64):
    template = """
        <!DOCTYPE html>
        <ul id="outputs">
            {% for o in appOutputs %}
              <li>
                {{ o.name }}: {{o.value}}
              </li>
            {% endfor %}
        </ul>
        <div>
            <a href="{{b64}}">Download .xlsx</a>
        </div>
    """
    outputTags = "".join([f"""<div>{o['name']}</div>""" for o in appOutputs])
    # note strip() important as gi checks for 'html>' in file_string[:20]
    rtemplate = Environment(loader=BaseLoader).from_string(template.strip())
    html = rtemplate.render(appOutputs=appOutputs, b64=b64)
    return html


def runApp(inputUrl, outputUrl):
    """
        fetch Giraffe payload, run app and post repsonse back to giraffe
    """
    payload = requests.get(inputUrl).json()
    print(inputUrl)
    print(payload)

    inputs, outputs = giraffeToExcel(payload)
    appOutputs = excel.compute(XLSX_FILE, inputs, outputs)
    b64 = encodeXLSX(XLSX_FILE, inputs)
    html = makeHTML(appOutputs, b64)

    output = {"appData": {"output": html}}
    print(output)
    print(outputUrl)

    ret = requests.post(outputUrl, json=output)
    print(ret.text)


if __name__ == "__main__":

    with open("demo/payload.json") as f:
        payload = json.load(f)

    inputs, outputs = giraffeToExcel(payload)
    appOutputs = excel.compute(XLSX_FILE, inputs, outputs)
    b64 = encodeXLSX(XLSX_FILE, inputs)
    html = makeHTML(appOutputs, b64)

    with open("demo.html", "w") as f:
        f.write(html)
