# Giraffe - Excel example app

This project is an example of how to deploy a simple Excel powered app in Giraffe. The app takes a spreadsheet (demo/Financial Calculator.xlsx in this case), fills in the 'giraffe sections' tab with the projects data - then runs the spreadsheet and extracts the outputs specified in demo/outputs.json. It also returns the spreadsheet for download.

Giraffe apps run by placing messages on Google Cloud pubsub topic unique to that app. Each message has a temporary `input_url` and `output_url`. Apps can run as workers by subscribing to their topic. This project shows how they can run by deploying an endpoint to which the topic will push messages. 

## Demos
Just run a spreadsheet directly specifying input cells
```
python3 demo/simple.py
```

Run a spreadsheet based on Giraffe data
```
python3 demo/giraffe.py
```

##  Deploy app to Cloud Run (rerun this for subsequent deploys)
```
PROJECT_ID=<YOUR GOOGLE APP ENGINE PROJECT ID>
NAME=excel-app
PSMA_API_KEY=<ENV VARIABLE REQUIRED BY YOUR APP>

gcloud builds submit --tag gcr.io/$PROJECT_ID/$NAME
gcloud run deploy $NAME --image gcr.io/$PROJECT_ID/$NAME --platform managed
```

